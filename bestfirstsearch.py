class node:
	def __init__(self,name):
		self.name=name
		self.vertex=[]
		self.range=[]
		self.heuristic=0

	def conn(self,x,range):
		self.vertex.append(x)
		self.range.append(range)

	def postheuristic(self,n):
		self.heuristic=n

def bestfs(initial,goal):
	_open=[]
	_close=[]
	_open.append(initial)
	while len(_open)>0:
		temp=_open
		best=_open[0]
		
		for i in _open:
			if i.heuristic<best.heuristic:
				best=i
		
		del _open[:]
		_close.append(best)
		
		if best==goal:
			return _close
		
		for i in temp:
			if _close.count(i)==0:
				_open.append(i)

		for i in best.vertex:
			if _open.count(i)==0:
				_open.append(i)

graph=[]
graph.append(node("S")) #0
graph[0].postheuristic(80)

graph.append(node("A")) #1
graph[1].postheuristic(80)

graph.append(node("B")) #2
graph[2].postheuristic(60)

graph.append(node("C")) #3
graph[3].postheuristic(70)

graph.append(node("D")) #4
graph[4].postheuristic(85)

graph.append(node("E")) #5
graph[5].postheuristic(74)

graph.append(node("F")) #6
graph[6].postheuristic(70)

graph.append(node("H")) #7
graph[7].postheuristic(40)

graph.append(node("J")) #8
graph[8].postheuristic(100)

graph.append(node("L")) #9
graph[9].postheuristic(20)

graph.append(node("M")) #10
graph[10].postheuristic(70)

graph.append(node("K")) #11
graph[11].postheuristic(30)

graph.append(node("G")) #12
graph[12].postheuristic(0)


graph[0].conn(graph[1],10)
graph[0].conn(graph[2],25)
graph[0].conn(graph[3],30)
graph[0].conn(graph[4],35)
graph[0].conn(graph[5],10)

graph[1].conn(graph[12],90)

graph[2].conn(graph[1],10)
graph[2].conn(graph[6],5)
graph[2].conn(graph[11],50)

graph[3].conn(graph[7],40)

graph[4].conn(graph[7],25)
graph[4].conn(graph[9],52)

graph[5].conn(graph[8],20)

graph[6].conn(graph[11],20)

graph[7].conn(graph[9],25)

graph[8].conn(graph[10],40)

graph[9].conn(graph[12],40)

graph[10].conn(graph[12],80)

graph[11].conn(graph[12],30)

a=bestfs(graph[0],graph[12])

jml=0

for i in range(len(a)):
	if i<len(a)-1:
		jml=jml+a[i].range[a[i].vertex.index(a[i+1])]
	print a[i].name

print "Dengan jarak = "+str(jml)
