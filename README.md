## UCS (Unified Cost Search)

Algoritma search untuk mencari langkah terpendek untuk mencapai suatu titik, menggunakan metode BFS(Breadth First Search).Termasuk dalam metode pencarian blind.

Algorithm :

	1. Menambah node asal kedalam queue
	2. Tandai node visited
	3. Perulangan hingga semua simpul yang berhubungan dengan node habis :
		a. deque queue
		b. apabila node deque bukan visited:
			- apabila node tersebut adalah Goal, selesai dengan node tersebut sebagai solus.
			Jika bukan tambahkan kedalam queue
		c. Baca node yang terhubung berikutnya
	4. Jika tidak ada jalur yang berhubungan dengan Goal, selesai dan tidak ada solusi.

## Simulated Annealing

Algoritma search yang termasuk dalam metode pencarian heuristic.
Menggunakan teori fisika untuk peleburan.

Algorithm :

	1. Inisialisasi nilai awal temperatur, dan persentase pendinginan
	2. inisialisasi solusi terbaik dengan solusi awal
	3. Perulangan hingga temperatur dibawah 1:
		a. inisialisasi new solution dengan solusi awal
		b. buat new solution dengan mengacak jalur solusi awal secara random
		c. lakukan testing apakah new solution bisa menggantikan solusi awal:
			- apabila jarak new solution lebih kecil dari solusi awal maka gantikan, jika tidak
			gunakan perhitungan probabilitas dengan rumus P(H2-H1)=e^(H2-H1)/T
		d. Jika solusi awal lebih kecil dari solusi terbaik maka inisialisasi solusi terbaik dengan solusi awal
	4. Keluaran berupa solusi terbaik

##Greedy Best First Search

Algoritma yang termasuk dalam pencarian heuristic dan salah satu metode dari best first search.
Dalam greedy algorithm menggunakan informasi heuristic, yaitu perkiraan jarak dengan menarik garis lurus dari sebuah node menuju node lainnya.

Algorithm :

	1. Inisialisasi OPEN dengan titik awal node
	2. Perulangan hingga OPEN kosong :
		a. Pilih node terbaik dari open dengan menggunakan nilai heuristic yang ada
		b. tambahkan node terbaik tersebut kedalam CLOSE
		c. Apabila node terbaik merupakan Goal, berhenti lakukan keluaran berupa CLOSE
		d. baca semua node yang terhubung dengan node terbaik :
			- apabila node tersebut belum ada dalam CLOSE maupun dalam OPEN tambahkan node tersebut ke dalam OPEN