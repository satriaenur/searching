import random
import math
import copy

class city:
	def __init__(self):
		self.x=random.uniform(50,200)
		self.y=random.uniform(50,200)

	def distance(self,city):
		xdist=abs(self.x-city.x);
		ydist=abs(self.y-city.y);
		dist=math.sqrt(math.pow(xdist,2)+math.pow(ydist,2))
		return dist

class tourmgr:
	def __init__(self):
		self.cities=[]

	def addcity(self,city):
		self.cities.append(city)


class _tour:
	def __init__(self,x):
		self.tour=[]
		self.distance=0
		for i in x.cities:
			self.tour.append(None)

	def clone(self,x):
		for i in range(len(x.tour)):
			self.tour[i]=x.tour[i]

	def setcity(self,jml,city):
		self.tour[jml]=city
		self.distance=0

	def generateIndividual(self,x):
		for i in range(len(x.cities)):
			self.setcity(i,x.cities[i])
		random.shuffle(self.tour)

	def getDistance(self):
		if(self.distance==0):
			td=0
			for i in range(len(self.tour)):
				cfrom=self.tour[i]

				if i+1<len(self.tour):
					dcity=self.tour[i+1]
				else:
					dcity=self.tour[0]

				td=td+cfrom.distance(dcity)
			self.distance=td
		return self.distance

	def traversal(self):
		gene="| "
		for i in range(len(self.tour)):
			gene=gene+str(self.tour[i].x)+", "+str(self.tour[i].y)+" | "
		print gene

def acceptanceProbability(energy,newEnergy,temperature):
	if(newEnergy<energy):
		return 1.0
	return math.exp((energy-newEnergy)/temperature)

tmgr=tourmgr()

for i in range(10):
	a=city()
	tmgr.addcity(a)

temp=10000

coolingRate=0.003

currSolution=_tour(tmgr)
currSolution.generateIndividual(tmgr)
print "Initial solution distance: "+str(currSolution.getDistance())
print "Initial Tour : "
currSolution.traversal()

best=_tour(tmgr)
best.clone(currSolution)
while temp>1:
	newsolution=_tour(tmgr)
	newsolution.clone(currSolution)

	tourpos1=int(len(newsolution.tour)*random.random())
	tourpos2=int(len(newsolution.tour)*random.random())
	citySwap1=newsolution.tour[tourpos1]
	citySwap2=newsolution.tour[tourpos2]

	newsolution.setcity(tourpos2,citySwap1)
	newsolution.setcity(tourpos1,citySwap2)

	currentEnergy=currSolution.getDistance()
	neigbourEnergy=newsolution.getDistance()
	mm=acceptanceProbability(currentEnergy,neigbourEnergy,temp)

	if mm>random.random():
		currSolution=_tour(tmgr)
		currSolution.clone(newsolution)

	if currSolution.getDistance()<best.getDistance():
		best=_tour(tmgr)
		best.clone(currSolution)

	temp=temp*(1-coolingRate)

print "Final solution distance : "+str(best.getDistance())
print "Tour : "
best.traversal()
