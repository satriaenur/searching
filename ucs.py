class node:
	def __init__(self,data):
		self.info=data
		self.next=None

class queue:
	def __init__(self):
		self.head=None
		self.tail=None

	def inqueue(self,data):
		p=self.head
		if(self.head==None):
			self.head=data
		else:
			self.tail.next=data
		self.tail=data

	def deque(self):
		p=self.head
		self.head=p.next
		return p

	def traversal(self):
		p=self.head
		while(p!=None):
			print (p.info)
			p=p.next


class graphnode:
	def __init__(self,data):
		self.info=data
		self.next=None
		self.nvertex=None
		self.visit=False

	def inservertex(self,data):
		p=self.nvertex
		if(p!=None):
			while(p.nvertex!=None):
				p=p.nvertex
			p.nvertex=data
		else:
			self.nvertex=data

class vertexnode:
	def __init__(self,data,weight):
		self.info=data
		self.nvertex=None
		self.weight=weight

class graph:
	def __init__(self):
		self.root=None

	def insertgraph(self,data):
		p=self.root
		if (p==None):
			self.root=data
		else:
			while(p.next!=None):
				p=p.next
			p.next=data

	def bfs(self):
		q=queue()
		p=self.root
		param=p.visit
		p.visit=p.visit+1
		q.inqueue(node(p))
		while(q.head!=None):
			vdeq=q.deque()
			print(vdeq.info.info)
			v=vdeq.info.nvertex
			while(v!=None):
				if(v.info.visit==param):
					q.inqueue(node(v.info))
					v.info.visit=v.info.visit+1
				v=v.nvertex

	def traversal(self):
		p=self.root
		while(p!=None):
			print(p.info)
			p=p.next

	def bfs(self):
		q=queue()
		p=self.root
		param=p.visit
		p.visit=p.visit+1
		print p.info
		q.inqueue(p)
		while(q.head!=None):
			r=q.deque().nvertex
			while(r!=None):
				if(r.info.visit==param):
					r.info.visit=r.info.visit+1
					print r.info.info
					q.inqueue(r.info)
				r=r.nvertex

	def dfs(self,node):
		if(node==None):
			return False
		node.visit=node.visit+1
		print (node.info)
		while(node.nvertex!=None):
			if(not(node.nvertex.info.visit)):
				self.dfs(node.nvertex.info)
			node=node.nvertex

	def ucs(self,fr,dest):
		q=queue()
		p=fr
		arr=[]
		visited=[]
		q.inqueue(node([p,p.info,0]))
		while(q.head!=None):
			deq=q.deque()
			visited.append(deq.info[0])
			v=deq.info[0].nvertex
			while(v!=None):
				ins=[v.info,deq.info[1]+'->'+v.info.info,deq.info[2]+v.weight]
				if(visited.count(v.info)==0):
					if(v.info.info==dest.info):
						arr.append(ins)
					else:
						q.inqueue(node(ins))
				v=v.nvertex

		if(len(arr)>0):
			a=arr[0]
			for i in range(len(arr)):
				if(a[2]>arr[i][2]):
					a=arr[i]
			print a[1]+" sejauh "+str(a[2])
		else:
			print "Tidak ada jalur"



gr=graph()
a=graphnode("A")
b=graphnode("B")
c=graphnode("C")
d=graphnode("D")
e=graphnode("E")
f=graphnode("F")
g=graphnode("G")

gr.insertgraph(a)
gr.insertgraph(b)
gr.insertgraph(c)
gr.insertgraph(d)
gr.insertgraph(e)
gr.insertgraph(f)
gr.insertgraph(g)

a.inservertex(vertexnode(b,5))
a.inservertex(vertexnode(d,3))

b.inservertex(vertexnode(a,5))
b.inservertex(vertexnode(c,6))
b.inservertex(vertexnode(d,5))
b.inservertex(vertexnode(e,5))
b.inservertex(vertexnode(f,5))

c.inservertex(vertexnode(b,6))	
c.inservertex(vertexnode(e,6))
c.inservertex(vertexnode(f,6))
c.inservertex(vertexnode(g,8))

d.inservertex(vertexnode(a,3))
d.inservertex(vertexnode(b,5))
d.inservertex(vertexnode(e,5))
d.inservertex(vertexnode(f,5))

e.inservertex(vertexnode(b,5))
e.inservertex(vertexnode(c,6))
e.inservertex(vertexnode(d,5))
e.inservertex(vertexnode(f,5))

f.inservertex(vertexnode(b,5))
f.inservertex(vertexnode(c,6))
f.inservertex(vertexnode(d,5))
f.inservertex(vertexnode(e,5))
f.inservertex(vertexnode(g,8))

g.inservertex(vertexnode(c,8))
g.inservertex(vertexnode(f,8))

gr.dfs(gr.root)
gr.ucs(a,g)
